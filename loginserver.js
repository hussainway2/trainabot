const config = require('./api/config/config');
const app = require('./api/config/express');
const postgresql = require('./api/config/postgresql')

async function startAPI() {

    try {

        await postgresql.connect()
        app.listen(config.port, () => {
            console.info(`Server started on port ${config.port} (${config.env})`);
        });
        return app

    } catch (error) {
        console.log('API ', new Error(error))
    }

}

module.exports = startAPI();