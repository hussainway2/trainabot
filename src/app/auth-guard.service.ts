import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private _StorageService: StorageService,
    private _Router: Router, private http: HttpClient) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this._StorageService.getToken("token")) {
      // this.socket.emit('disconnect')
      // this.socket.io();
      // this.socket = new io(this.SOCKET_URL)

      const token = this._StorageService.getToken("token");

      if (!token)
        this.logoutUser();

      return true;

    } else {
      return this._Router.navigate(['/login']);
    }
  }

  logoutUser() {
    this._StorageService.removeAll();
    this._Router.navigate(['/']);
  }
}
