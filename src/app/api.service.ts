import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {



  constructor(private _HttpClient: HttpClient,
    private _StorageService: StorageService,
    private router: Router,
    private authGuardService: AuthGuardService) { }


  LOGIN_URL = environment.loginUrl;
  API_URL=environment.API_URL;

  post(url, body = {}) {
    return this._HttpClient.post(this.LOGIN_URL + url, body);
  }
  get(url) {
    return this._HttpClient.get(this.API_URL + url);
  }
  postAuth(url, body = {}) {
    const token = this._StorageService.getToken("token");
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': token
    });
    let options = {
      headers: headers
    }
    return this._HttpClient.post(this.API_URL + url, body, options).pipe(
      map(response => {
        return response
      }),
      catchError(error => {
        if (error.statusText == "Unauthorized") {
          setTimeout(() => {
            this.authGuardService.logoutUser();
          }, 100)
        }
        return throwError(error)
      }),
    );
  }


  getAuth(url) {

    const token = this._StorageService.getToken("token");
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': token
    });
    let options = {
      headers: headers
    }

    return this._HttpClient.get(this.API_URL + url, options).pipe(
      map(response => {
        return response
      }),
      catchError(error => {
        if (error.statusText == "Unauthorized") {
          setTimeout(() => {
            this.authGuardService.logoutUser()
          }, 100)
        }
        return throwError(error)
      }),
    );;
  }

  // postAuth(url, body = {}) {
  //   const token = this._StorageService.getToken("token");
  //   const pmpToken = this._StorageService.getToken("access_token");
  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'x-access-token': token,
  //     'accesstoken': pmpToken
  //   });
  //   let options = {
  //     headers: headers
  //   }
  //   return this._HttpClient.post(this.API_URL + url, body, options).pipe(
  //     map(response => {
  //       return response
  //     }),
  //     catchError(error => {
  //       if (error.statusText == "Unauthorized") {
  //         setTimeout(() => {
  //           this.authGuardService.logoutUser();
  //         }, 100)
  //       }
  //       return throwError(error)
  //     }),
  //   );
  // }

  // getAuth(url) {

  //   const token = this._StorageService.getToken("token");
  //   const pmpToken = this._StorageService.getToken("access_token");
  //   let headers = new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'x-access-token': token,
  //     'accesstoken': pmpToken
  //   });
  //   let options = {
  //     headers: headers
  //   }

  //   return this._HttpClient.get(this.API_URL + url, options).pipe(
  //     map(response => {
  //       return response
  //     }),
  //     catchError(error => {
  //       if (error.statusText == "Unauthorized") {
  //         setTimeout(() => {
  //           this.authGuardService.logoutUser()
  //         }, 100)
  //       }
  //       return throwError(error)
  //     }),
  //   );;
  // }
}
