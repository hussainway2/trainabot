import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public ApiService: ApiService,public _StorageService:StorageService, private _Router: Router,) { }



  login : any = {}
  errorMesg : any = "";

  ngOnInit(): void {
  }

  onSubmitLogin(){
    this.errorMesg="";
    this.ApiService.post('user/login',this.login).subscribe((result) =>{
      console.log("result",result)

      if(result['success'] == true){
        this._StorageService.setToken("token", result['tokenKeys']['token']);
        this._Router.navigate(['dashboard'])
      }else{
        this.errorMesg= result['message']
      }
    })
  }

}
