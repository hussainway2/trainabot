import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _RestApi: ApiService) { }



  productsInfo: any;
  orders: any;
  showTable: boolean = false;

  ngOnInit(): void {
    this.getAllProducts()
  }

  getAllProducts() {
    this._RestApi.getAuth('ecom/products').subscribe((result) => {
      console.log("resut", result)
      if (result['success'] == true) {
        this.productsInfo = result['products']
      }
    })
  }

  onClickAddToCart(productInfo) {
    console.log("productInfo", productInfo.id)
    this._RestApi.postAuth('ecom/add-to-cart', productInfo).subscribe((result) => {
      if (result['success'] == true) {
        alert("Your Order Placed Sucessfully")
      } else {
        alert(result['message'])
      }
    })
  }
  onClickViewOrders() {
    this.showTable = !this.showTable;
    this._RestApi.getAuth('ecom/orders').subscribe((result) => {
      // console.log("orders",result)
      if (result['success'] == true) {
        this.orders = result['orders']
      } else {
      }
    })
  }
  onClickRunThread() {
    this._RestApi.getAuth('ecom/node-thred').subscribe((result) => {
      // console.log("orders",result)
      if (result['success'] == true) {
        alert('Thread Run Completed')
      } else {
        alert(result['message'])
      }
    })
  }

  onClickRunPyScript() {
    this._RestApi.getAuth('ecom/run-python').subscribe((result) => {
      console.log("orders",result)
      // if (result['success'] == true) {
      //   alert('Thread Run Completed')
      // } else {
      //   alert(result['message'])
      // }
    })
  }

}
