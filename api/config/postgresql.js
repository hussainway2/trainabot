
const { Pool } = require('pg')

var postgresOperation = module.exports;

let postgresConnection = null


postgresOperation.connect = function () {
    let options = {
        user: 'postgres',
        host: '127.0.0.1',
        database: 'postgres',
        password: 'qwerty',
        port: 5432,
    }
    postgresConnection = new Pool(options)
  
    return postgresConnection;

}