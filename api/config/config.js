const Joi = require('@hapi/joi');
require('dotenv').config();


const envSchema = Joi.object({
    NODE_ENV: Joi.string().valid('production','development','test','provision').default('development'),
    SERVER_PORT: Joi.number().default(4040),
    JWT_SECRET: Joi.string().required().description('JWT Secret required to sign'),
}).unknown().required();


const { error, value: envVars } = envSchema.validate(process.env);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}


const config = {
    env: envVars.NODE_ENV,
    port: envVars.SERVER_PORT,
    jwtSecret: envVars.JWT_SECRET
};

module.exports = config;
