var productService=require('../services/productService')
var cartService=require('../services/cartService')
const express = require('express');
const router = express.Router();
const verfiy= require('../middlewares/middleware')


router.get('/products',verfiy,productService.getAllProducts);
router.post('/add-to-cart',verfiy,cartService.addToCart);
router.get('/orders',verfiy,cartService.getOrders);
router.get('/node-thred',verfiy,cartService.runThread);
router.get('/run-python',verfiy,cartService.runPythonScript);


module.exports = router;