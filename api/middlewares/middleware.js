const JWT = require('jsonwebtoken')
const config = require('../config/config')

const verify = function(request, response, next) {
    const token = request.headers['x-access-token']
        // console.log("token===>",token);
    JWT.verify(token, config.jwtSecret, function(token_auth_failed, token_auth_success) {
        // console.log("token_auth_success", token_auth_success)
        if (token_auth_failed) {
            return response.status(401).send({
                status: 'INVALID-AUTHENTICATION',
                message: 'Please login'
            })
        }
        request.user = token_auth_success
            // console.log("request.user====>",request.user);
        return next()
    })

}

module.exports = verify;
