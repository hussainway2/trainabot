var userService = module.exports;
var config = require('../config/config');
var posgresSql =require('../config/postgresql').connect();
const jwt = require('jsonwebtoken');
let posgresSqlCilent;
userService.login = async (req, res) => {
    if(!posgresSqlCilent)
        posgresSqlCilent=await posgresSql.connect()
    let { email, password } = req.body;
    let query = `SELECT * FROM accounts where email='${email}'`
    posgresSql.query(query, (err, result) => {
        if (err)
            return res.status(400).send({ message: "Something went worng" })
        else {
            if (result.rows.length == 0) return res.status(200).send({ message: "Email not exist", success: false });

            let accountInfo = result.rows[0];

            if (accountInfo.password != password) return res.status(200).send({ message: "Password wrong", success: false });

            let tokenKeys = {
                u_id: accountInfo.user_id,
                email: accountInfo.email
            }
            tokenKeys.token = jwt.sign(tokenKeys, config.jwtSecret)

            return res.status(200).send({ 'message': "Login Sucess", success: true, 'tokenKeys': tokenKeys })
        }
    })
}

