var config = require('../config/config');
var posgresSql = require('../config/postgresql').connect();
const {
    isMainThread, Worker, MessageChannel
} = require('worker_threads');

var cartService = module.exports;

let posgresSqlCilent;

cartService.addToCart = async (req, res) => {
    console.log("req", req.user)
    let date = new Date().getFullYear() + "-" + new Date().getMonth() + "-" + new Date().getDate()
    console.log("date", date.toString())
    let query = `INSERT INTO orders (u_id,id,orderdate) values (${req.user.u_id},${req.body.id},'${date}')`
    console.log("query", query)
    if (!posgresSqlCilent)
        posgresSqlCilent = await posgresSql.connect()
    posgresSql.query(query, (err, result) => {
        if (err) {
            console.log("err", err);
            return res.status(400).send({ message: "Something went worng" })
        }
        else {
            return res.status(200).send({ 'message': "product buy sucess", success: true })
        }
    })
}

cartService.getOrders = async (req, res) => {
    let query = `SELECT t1.name,t1.price,t2.orderdate from products as t1 JOIN orders as t2 on t1.id=t2.id where t2.u_id=${req.user.u_id}`

    console.log("query", query)

    if (!posgresSqlCilent)
        posgresSqlCilent = await posgresSql.connect()
    posgresSql.query(query, (err, result) => {
        if (err) {
            console.log("err", err);
            return res.status(400).send({ message: "Something went worng" })
        }
        else {
            return res.status(200).send({ 'message': "orders fetched sucess", success: true, orders: result.rows })
        }
    })
}

cartService.runThread = function (req, res) {

    if (isMainThread) {
        const worker = new Worker(`${__dirname}/worker.js`);
        const subchannel = new MessageChannel();
        subchannel.port2.onmessage('message', (message) => {
            // console.log("message", message)
        })

        worker.postMessage({ port: subchannel.port2 }, [subchannel.port2])

        worker.on('message', (message) => {
            // console.log("message --->", message)
            return res.status(200).send({ message: "success", success: true })
        });
    };

    // return res.status(200).send({ message: "success", success: true })
}


cartService.runPythonScript = function (req, res) {



    console.log("req.user", req.user)

    var spawn = require("child_process").spawn;


    // console.log("spawn",spawn)

    var process = spawn('python3', ['/home/exe0070/Desktop/trainabot/api/services/hello.py']);

    process.stdout.on('data', function (data) {
        console.log("data", data.toString())
    })

    return res.status(200).send({ message: "success", success: true })
}