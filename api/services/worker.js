const {
    parentPort, MessagePort
} = require('worker_threads');

parentPort.on('message', (message) => {
    console.log("Starting thread operation");
    let i=0;
    while (i < 1000000) {
        let j = 0;
        while (j < 100000) {
            j++;
        }
        i++;
    }
    console.log("Ending thread operation");
    parentPort.postMessage('Operation done');
    parentPort.close()
    
})