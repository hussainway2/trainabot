const config = require('./api/config/config');
const app = require('./api/config/express_backend');
const postgresql = require('./api/config/postgresql')

async function startAPI() {

    try {

        await postgresql.connect()
        app.listen(3000, () => {
            console.info(`Server started on port 3000`);
        });
        return app

    } catch (error) {
        console.log('API ', new Error(error))
    }

}

module.exports = startAPI();